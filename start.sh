result_dir=$HOME/ToneAgent/results
sync_dir=$HOME/ToneAgent/sync_results
scripts_dir=$HOME/ToneAgent/scripts
logs_dir=$HOME/ToneAgent/logs
config_dir=$HOME/ToneAgent

mkdir -p $result_dir $sync_dir $scripts_dir $logs_dir

# -----------SW adaptation---------------

str_for_arch=$(lscpu|head -n 1)
result=$(echo $str_for_arch | grep sw_64)

# 获取GOPATH和GOROOT
GO_PATH=$(go env|grep GOPATH|awk -F '=' '{print$2}'|awk -F '"' '{print $2}')
GO_ROOT=$(go env|grep GOROOT|awk -F '=' '{print$2}'|awk -F '"' '{print $2}')
CURRENT_DIR=$(pwd)

if [ "$result" != "" ]

then

echo "SW adaptation for sys"

cd $GO_ROOT/src/cmd/vendor/golang.org/x/sys && go mod init

cd $CURRENT_DIR

go mod edit -replace=golang.org/x/sys=$GO_ROOT/src/cmd/vendor/golang.org/x/sys

fi

# --------------------------------------

go mod tidy

# -----------SW adaptation---------------

str_for_arch=$(lscpu|head -n 1)
result=$(echo $str_for_arch | grep sw_64)
if [ "$result" != "" ]

then

echo "SW adaptation for procfs"

# 适配prometheus/procfs@v0.1.3
# 新增cpuinfo_SW64.go文件
echo "package procfs

var parseCPUInfo=parseCPUInfoSW64" > $GO_PATH/pkg/mod/github.com/prometheus/procfs@v0.1.3/cpuinfo_SW64.go


# cpuinfo.go文件添加sw64方法

match=$(grep -c parseCPUInfoSW64 $GO_PATH/pkg/mod/github.com/prometheus/procfs@v0.1.3/cpuinfo.go)

if [ "$match" == '0' ]; then
   echo 'func parseCPUInfoSW64(info []byte) ([]CPUInfo, error) {
        scanner := bufio.NewScanner(bytes.NewReader(info))

        // find the first "processor" line
        firstLine := firstNonEmptyLine(scanner)
        if !strings.HasPrefix(firstLine, "system type") || !strings.Contains(firstLine, ":") {
                return nil, errors.New("invalid cpuinfo file: " + firstLine)
        }
	field := strings.SplitN(firstLine, ": ", 2)
        cpuinfo := []CPUInfo{}
        systemType := field[1]

        i := 0

        for scanner.Scan() {
                line := scanner.Text()
                if !strings.Contains(line, ":") {
                        continue
                }
                field := strings.SplitN(line, ": ", 2)
                switch strings.TrimSpace(field[0]) {
                case "processor":
                        v, err := strconv.ParseUint(field[1], 0, 32)
                        if err != nil {
                                return nil, err
                        }
                        i = int(v)
                        cpuinfo = append(cpuinfo, CPUInfo{}) // start of the next processor
                        cpuinfo[i].Processor = uint(v)
                        cpuinfo[i].VendorID = systemType
                case "cpu model":
                        cpuinfo[i].ModelName = field[1]
                case "BogoMIPS":
                        v, err := strconv.ParseFloat(field[1], 64)
                        if err != nil {
                                return nil, err
                        }
                        cpuinfo[i].BogoMips = v
                }
        }
	return cpuinfo, nil
}
' >> $GO_PATH/pkg/mod/github.com/prometheus/procfs@v0.1.3/cpuinfo.go

fi

fi
# -----------------------------------------

go build
cp ./tone-agent $HOME/ToneAgent
cd $HOME/ToneAgent
echo "beego:
  AppName: toneagent
  RunMode: dev
  StaticDir: down1
  DirectoryIndex: true
  HttpAddr: 0.0.0.0
  CopyRequestBody: true
  HttpPort: 8479

result:
  ResultFileDir: results
  WaitingSyncResultDir: sync_results
  TmpScriptFileDir: scripts
  LogFileDir: logs
  LogFileName: toneagent.log


mode: active
tsn: tone20210101-001
proxy: https://tone-agent.openanolis.cn" > config.yaml

echo "ToneAgent start ..."
nohup ./tone-agent >> toneagent.log 2>&1 &
# ./tone-agent




